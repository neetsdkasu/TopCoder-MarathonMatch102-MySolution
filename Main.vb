Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Try
            Dim _trucksAndCouriers As New TrucksAndCouriers()

            ' do edit codes if you need

            CallPlanShipping(_trucksAndCouriers)

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub
    
    Function CallPlanShipping(_trucksAndCouriers As TrucksAndCouriers) As Integer
        Dim truckFixed As Integer = CInt(Console.ReadLine())
        Dim truckVariable As Integer = CInt(Console.ReadLine())
        Dim _warehouseXSize As Integer = CInt(Console.ReadLine()) - 1
        Dim warehouseX(_warehouseXSize) As Integer
        For _idx As Integer = 0 To _warehouseXSize
            warehouseX(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _warehouseYSize As Integer = CInt(Console.ReadLine()) - 1
        Dim warehouseY(_warehouseYSize) As Integer
        For _idx As Integer = 0 To _warehouseYSize
            warehouseY(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _warehouseItemSize As Integer = CInt(Console.ReadLine()) - 1
        Dim warehouseItem(_warehouseItemSize) As Integer
        For _idx As Integer = 0 To _warehouseItemSize
            warehouseItem(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _warehouseQuantitySize As Integer = CInt(Console.ReadLine()) - 1
        Dim warehouseQuantity(_warehouseQuantitySize) As Integer
        For _idx As Integer = 0 To _warehouseQuantitySize
            warehouseQuantity(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _customerXSize As Integer = CInt(Console.ReadLine()) - 1
        Dim customerX(_customerXSize) As Integer
        For _idx As Integer = 0 To _customerXSize
            customerX(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _customerYSize As Integer = CInt(Console.ReadLine()) - 1
        Dim customerY(_customerYSize) As Integer
        For _idx As Integer = 0 To _customerYSize
            customerY(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _customerItemSize As Integer = CInt(Console.ReadLine()) - 1
        Dim customerItem(_customerItemSize) As Integer
        For _idx As Integer = 0 To _customerItemSize
            customerItem(_idx) = CInt(Console.ReadLine())
        Next _idx

        Dim _result As String() = _trucksAndCouriers.planShipping(truckFixed, truckVariable, warehouseX, warehouseY, warehouseItem, warehouseQuantity, customerX, customerY, customerItem)

        Console.WriteLine(_result.Length)
        For Each _it As String In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function

End Module