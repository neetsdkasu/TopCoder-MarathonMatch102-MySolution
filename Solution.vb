Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)
Imports TpIIIs = System.Tuple(Of Integer, Integer, Integer())
Imports TpIIIsIs = System.Tuple(Of Integer, Integer, Integer(), Integer())

Public Class TrucksAndCouriers

    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)

    Dim cusXs() As Integer
    Dim cusYs() As Integer
    Dim cusIs() As Integer
    Dim fCost As Integer = 0, vCost As Integer = 0

    Dim dxs() As Integer = {0, 1,  0, -1}
    Dim dys() As Integer = {1, 0, -1,  0}

    Dim warehouseIds As New Dictionary(Of Integer, Integer)()
    Dim warehousePos As New List(Of Integer)()

    Public Function planShipping(truckFixed As Integer, truckVariable As Integer, warehouseX As Integer(), warehouseY As Integer(), warehouseItem As Integer(), warehouseQuantity As Integer(), customerX As Integer(), customerY As Integer(), customerItem As Integer()) As String()

        startTime = Environment.TickCount
        limitTime = startTime + 9000
        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))

        fCost = truckFixed
        vCost = truckVariable
        cusXs = customerX
        cusYs = customerY
        cusIs = customerItem

        Dim items(100) As Dictionary(Of Integer, Integer)
        For i As Integer = 0 To UBound(warehouseX)
            Dim id As Integer = warehouseItem(i)
            Dim dic As Dictionary(Of Integer, Integer) = items(id)
            If dic Is Nothing Then
                dic = New Dictionary(Of Integer, Integer)()
                items(id) = dic
            End If
            Dim k As Integer = MakeKey(warehouseX(i), warehouseY(i))
            Dim cnt As Integer = 0
            If Not dic.TryGetValue(k, cnt) Then
                cnt = 0
            End If
            cnt += warehouseQuantity(i)
            dic(k) = cnt
            If Not warehouseIds.ContainsKey(k) Then
                Dim wid As Integer = warehouseIds.Count
                warehouseIds(k) = wid
                warehousePos.Add(k)
            End If
        Next i

        Dim people(100) As List(Of Integer)

        For i As Integer = 0 To UBound(customerItem)
            Dim id As Integer = customerItem(i)
            If people(id) Is Nothing Then people(id) = New List(Of Integer)()
            people(id).Add(i)
        Next i

        Dim cusList(UBound(customerItem)) As Customer

        For i As Integer = 0 To UBound(people)
            If people(i) Is Nothing Then Continue For
            If items(i) Is Nothing Then Continue For

            Select Case items(i).Count
            Case 1
                Apply1(cusList, i, people(i), items(i))
            Case 2
                Apply2(cusList, i, people(i), items(i))
            Case 3
                Apply3(cusList, i, people(i), items(i))
            Case Else
                Throw New Exception("Unreachable")
            End Select

        Next i

        Dim posList(1001 * 1001 - 1) As Integer
        For y As Integer = 0 To 1000
            For x As Integer = 0 To 1000
                posList(y * 1001 + x) = MakeKey(x, y)
            Next x
        Next y

        Dim bestRet As Tuple(Of List(Of TpIIIs), Customer(), Integer) = Nothing
        Dim limTim As Integer = startTime + 8000
        For i As Integer = 1 To 10
            If Environment.TickCount > limTim Then Exit For
            Dim msRet As Tuple(Of List(Of TpIIIs), Customer(), Integer) = MultipleStart(posList, cusList, limTim)
            If bestRet Is Nothing OrElse msRet.Item3 < bestRet.Item3 Then
                bestRet = msRet
            End If
        Next i

        Dim trucks As List(Of TpIIIs) = bestRet.Item1
        cusList = bestRet.Item2

        Dim flag(trucks.Count - 1) As Boolean
        Dim trucks2 As New List(Of TpIIIsIs)()
        Shuffle(posList)
        For Each pos As Integer In posList
            If Environment.TickCount > limitTime Then Exit For
            Dim pX As Integer = GetX(pos)
            Dim pY As Integer = GetY(pos)
            Dim tcnt(warehouseIds.Count - 1) As List(Of Integer)
            Dim ccnt(warehouseIds.Count - 1) As List(Of Customer)
            Dim bCosts(warehouseIds.Count - 1) As Integer
            Dim aCosts(warehouseIds.Count - 1) As Integer
            Dim tcc As Integer = 0
            Dim ccc As Integer = 0
            For i As Integer = 0 To trucks.Count - 1
                If flag(i) Then Continue For
                Dim t As TpIIIs = trucks(i)
                Dim sX As Integer = GetX(t.Item1)
                Dim sY As Integer = GetY(t.Item1)
                Dim eX As Integer = GetX(t.Item2)
                Dim eY As Integer = GetY(t.Item2)
                Dim d1 As Integer = Distance(sX, sY, eX, eY)
                Dim d2 As Integer = Distance(pX, pY, eX, eY)
                If d1 <= d2 Then Continue For
                Dim wid As Integer = warehouseIds(t.Item1)
                If tcnt(wid) Is Nothing Then tcnt(wid) = New List(Of Integer)()
                tcnt(wid).Add(i)
                bCosts(wid) += d1
                aCosts(wid) += d2
                tcc = Math.Max(tcc, tcnt(wid).Count)
            Next i
            For Each c As Customer In cusList
                If c.Junction Then Continue For
                Dim d1 As Integer = Distance(GetX(c.Target), GetY(c.Target), cusXs(c.Id), cusYs(c.Id))
                Dim d2 As Integer = Distance(pX, pY, cusXs(c.Id), cusYs(c.Id))
                If d1 <= d2 Then Continue For
                Dim wid As Integer = warehouseIds(c.Target)
                If ccnt(wid) Is Nothing Then ccnt(wid) = New List(Of Customer)()
                ccnt(wid).Add(c)
                bCosts(wid) += d1
                aCosts(wid) += d2
                If tcnt(wid) Is Nothing Then
                    ccc = Math.Max(ccc, ccnt(wid).Count)
                Else
                    tcc = Math.Max(tcc, tcnt(wid).Count + ccnt(wid).Count)
                End If
            Next c
            If tcc < 2 AndAlso ccc <= fCost Then Continue For
            For i As Integer = 0 To UBound(tcnt)
                If tcnt(i) Is Nothing Then
                    If ccnt(i) Is Nothing OrElse ccnt(i).Count <= fCost Then
                        Continue For
                    End If
                ElseIf ccnt(i) Is Nothing Then
                    If tcnt(i).Count < 2 Then
                        Continue For
                    End If
                End if
                
                Dim sX As Integer = GetX(warehousePos(i))
                Dim sY As Integer = GetY(warehousePos(i))
                Dim d As Integer = Distance(sX, sY, pX, pY)
                If bCosts(i) <= fCost + vCost * d + aCosts(i) Then Continue For
                Dim cs As New List(Of Integer)
                Dim ts(-1) As Integer
                If ccnt(i) IsNot Nothing Then
                    For Each c As Customer In ccnt(i)
                        c.Junction = True
                        c.Target = pos
                        cs.Add(c.Id)
                    Next c
                End If
                If tcnt(i) IsNot Nothing Then
                    For Each ti As Integer In tcnt(i)
                        Dim t As TpIIIs = trucks(ti)
                        trucks(ti) = New TpIIIs(pos, t.Item2, t.Item3)
                        flag(ti) = True
                    Next ti
                    ts = tcnt(i).ToArray()
                End If
                trucks2.Add(New TpIIIsIs(warehousePos(i), pos, ts, cs.ToArray()))
            Next i
        Next pos

        For i As Integer = 0 To trucks2.Count - 1
            Dim t2 As TpIIIsIs = trucks2(i)
            Dim sX As Integer = GetX(t2.Item1)
            Dim sY As Integer = GetY(t2.Item1)
            Dim eX As Integer = GetX(t2.Item2)
            Dim eY As Integer = GetY(t2.Item2)
            Dim selX As Integer = eX, selY As Integer = eY
            Dim selCost As Integer = vCost * Distance(sX, sY, eX, eY)
            For Each tid As Integer In t2.Item3
                Dim t1 As TpIIIs = trucks(tid)
                selCost += Distance(eX, eY, GetX(t1.Item2), GetY(t1.Item2))
            Next tid
            For Each cid As Integer In t2.Item4
                selCost += Distance(eX, eY, cusXs(cid), cusYs(cid))
            Next cid
            For q As Integer = 0 To 1000
                For j As Integer = 0 To UBound(dxs)
                    Dim x As Integer = eX + dxs(j)
                    Dim y As Integer = eY + dys(j)
                    Dim tmpCost As Integer = vCost * Distance(sX, sY, x, y)
                    For Each tid As Integer In t2.Item3
                        Dim t As TpIIIs = trucks(tid)
                        tmpCost += Distance(x, y, GetX(t.Item2), GetY(t.Item2))
                    Next tid
                    For Each cid As Integer In t2.Item4
                        tmpCost += Distance(x, y, cusXs(cid), cusYs(cid))
                    Next cid
                    If tmpCost < selCost Then
                        selCost = tmpCost
                        selX = x
                        selY = y
                    End If
                Next j
                If selX = eX AndAlso selY = eY Then Exit For
                eX = selX
                eY = selY
            Next q
            Dim pos As Integer = MakeKey(selX, selY)
            If pos = t2.Item2 Then Continue For
            trucks2(i) = New TpIIIsIs(t2.Item1, pos, t2.Item3, t2.Item4)
            For Each tid As Integer In t2.Item3
                Dim t1 As TpIIIs = trucks(tid)
                trucks(tid) = New TpIIIs(pos, t1.Item2, t1.Item3)
            Next tid
            For Each cid As Integer In t2.Item4
                cusList(cid).Target = pos
            Next cid
        Next i


        Dim ret As New List(Of String)()

        For Each t As TpIIIsIs In trucks2
            Dim sX As Integer = GetX(t.Item1)
            Dim sY As Integer = GetY(t.Item1)
            Dim eX As Integer = GetX(t.Item2)
            Dim eY As Integer = GetY(t.Item2)
            Dim ships As New List(Of Integer)()
            For Each ti As Integer In t.Item3
                For Each id As Integer In trucks(ti).Item3
                    ships.Add(cusIs(id))
                Next id
            Next ti
            For Each id As Integer In t.Item4
                ships.Add(cusIs(id))
            Next id
            ret.Add(CommandTWithItems(sX, sY, eX, eY, ships.ToArray()))
        Next t

        For Each t As TpIIIs In trucks
            Dim sX As Integer = GetX(t.Item1)
            Dim sY As Integer = GetY(t.Item1)
            Dim eX As Integer = GetX(t.Item2)
            Dim eY As Integer = GetY(t.Item2)
            ret.Add(CommandT(sX, sY, eX, eY, t.Item3))
        Next t

        For Each c As Customer In cusList
            Dim sX As Integer = GetX(c.Target)
            Dim sY As Integer = GetY(c.Target)
            Dim eX As Integer = cusXs(c.Id)
            Dim eY As Integer = cusYs(c.Id)
            Dim id As Integer = customerItem(c.Id)
            ret.Add(CommandC(sX, sY, eX, eY, id))
        Next c

        planShipping = ret.ToArray()

        Console.Error.WriteLine("total time: {0}", Environment.TickCount - startTime)

    End Function

    Private Function MakeKey(X As Integer, Y As Integer) As Integer
        MakeKey = (Y << 10) Or X
    End Function
    Private Function GetX(k As Integer) As Integer
        GetX = k And ((1 << 10) - 1)
    End Function
    Private Function GetY(k As Integer) As Integer
        GetY = k >> 10
    End Function

    Private Function Distance(X1 As Integer, Y1 As Integer, X2 As Integer, Y2 As Integer) As Integer
        Distance = Math.Abs(X2 - X1) + Math.Abs(Y2 - Y1)
    End Function

    Private Function CommandC(sX As Integer, sY As Integer, eX As Integer, eY As Integer, id As Integer) As String
        CommandC = String.Format("C,{0},{1},{2},{3},{4}", sX, sY, eX, eY, id)
    End Function

    Private Function CommandT(sX As Integer, sY As Integer, eX As Integer, eY As Integer, ids() As Integer) As String
        Dim ret As String = String.Format("T,{0},{1},{2},{3}", sX, sY, eX, eY)
        For Each id As Integer In ids
            ret += "," + cusIs(id).ToString()
        Next id
        CommandT = ret
    End Function

    Private Function CommandTWithItems(sX As Integer, sY As Integer, eX As Integer, eY As Integer, ids() As Integer) As String
        Dim ret As String = String.Format("T,{0},{1},{2},{3}", sX, sY, eX, eY)
        For Each id As Integer In ids
            ret += "," + id.ToString()
        Next id
        CommandTWithItems = ret
    End Function

    Private Function InRange(r1 As Integer, r2 As Integer, tg As Integer) As Boolean
        InRange = (r1 <= tg AndAlso tg <= r2) OrElse (r2 <= tg AndAlso tg <= r1)
    End Function

    Private Function IsInner(c As Customer, pos As Integer) As Boolean
        Dim sX As Integer = GetX(c.Target)
        Dim sY As Integer = GetY(c.Target)
        Dim eX As Integer = cusXs(c.Id)
        Dim eY As Integer = cusYs(c.Id)
        Dim pX As Integer = GetX(pos)
        Dim pY As Integer = GetY(pos)
        IsInner = InRange(sX, eX, pX) AndAlso InRange(sY, eY, pY)
    End Function

    Private Sub Apply1(ret() As Customer, id As Integer, people As List(Of Integer), items As Dictionary(Of Integer, Integer))
        Dim kv As KeyValuePair(Of Integer, Integer) = items.First()
        ' Dim sX As Integer = GetX(kv.Key)
        ' Dim sY As Integer = GetY(kv.Key)
        For Each i As Integer In people
            ret(i) = New Customer(i, kv.Key)
        Next i
    End Sub

    Private Sub Apply2(ret() As Customer, id As Integer, people As List(Of Integer), items As Dictionary(Of Integer, Integer))
        Dim pairs As New List(Of Tp)()
        Dim keys() As Integer = items.Keys.ToArray()
        Dim k1 As Integer = keys(0), k2 As Integer = keys(1)
        Dim v1 As Integer = items(k1), sX1 As Integer = GetX(k1), sY1 As Integer = GetY(k1)
        Dim v2 As Integer = items(k2), sX2 As Integer = GetX(k2), sY2 As Integer = GetY(k2)
        For Each i As Integer In people
            Dim d1 As Integer = Distance(sX1, sY1, cusXs(i), cusYs(i))
            Dim d2 As Integer = Distance(sX2, sY2, cusXs(i), cusYs(i))
            pairs.Add(New Tp(-Math.Abs(d1 - d2), i))
        Next i
        pairs.Sort()
        For Each p As Tp In pairs
            Dim i As Integer = p.Item2
            Dim d1 As Integer = Distance(sX1, sY1, cusXs(i), cusYs(i))
            Dim d2 As Integer = Distance(sX2, sY2, cusXs(i), cusYs(i))
            If d1 < d2 AndAlso v1 > 0 Then
                ret(i) = New Customer(i, k1)
                v1 -= 1
            ElseIf d2 < d1 AndAlso v2 > 0 Then
                ret(i) = New Customer(i, k2)
                v2 -= 1
            ElseIf v1 > v2 Then
                ret(i) = New Customer(i, k1)
                v1 -= 1
            Else
                ret(i)  = New Customer(i, k2)
                v2 -= 1
            End If
        Next p
    End Sub

    Private Sub Apply3(ret() As Customer, id As Integer, people As List(Of Integer), items As Dictionary(Of Integer, Integer))
        Dim tmp1 As New List(Of Tp)(), tmp2 As New List(Of Tp)(), tmp3 As New List(Of Tp)()
        Dim keys() As Integer = items.Keys.ToArray()
        Dim k1 As Integer = keys(0), k2 As Integer = keys(1), k3 As Integer = keys(2)
        Dim v1 As Integer = items(k1), sX1 As Integer = GetX(k1), sY1 As Integer = GetY(k1)
        Dim v2 As Integer = items(k2), sX2 As Integer = GetX(k2), sY2 As Integer = GetY(k2)
        Dim v3 As Integer = items(k3), sX3 As Integer = GetX(k3), sY3 As Integer = GetY(k3)

        For Each i As Integer In people
            If v1 > 0 Then
                Dim d1 As Integer = Distance(sX1, sY1, cusXs(i), cusYs(i))
                tmp1.Add(New Tp(d1, i))
                v1 -= 1
            ElseIf v2 > 0 Then
                Dim d2 As Integer = Distance(sX2, sY2, cusXs(i), cusYs(i))
                tmp2.Add(New Tp(d2, i))
                v2 -= 1
            ElseIf v3 > 0 Then
                Dim d3 As Integer = Distance(sX3, sY3, cusXs(i), cusYs(i))
                tmp3.Add(New Tp(d3, i))
                v3 -= 1
            Else
                Throw New Exception("Unreachable")
            End If
        Next i
        Dim dummy As New Tp(0, -1)
        For i As Integer = 1 To v1
            tmp1.Add(dummy)
        Next i
        For i As Integer = 1 To v2
            tmp2.Add(dummy)
        Next i
        For i As Integer = 1 To v3
            tmp3.Add(dummy)
        Next i

        Do
            Dim time1 As Integer = Environment.TickCount
            If time1 > limitTime Then Exit Do

            Dim exitFlag As Boolean = True
            For t1 As Integer = 0 To tmp1.Count - 1
            For t2 As Integer = 0 To tmp2.Count - 1
            For t3 As Integer = 0 To tmp3.Count - 1
                Dim p1 As Tp = tmp1(t1)
                Dim d1 As Integer = p1.Item1, c1 As Integer = p1.Item2
                Dim p2 As Tp = tmp2(t2)
                Dim d2 As Integer = p2.Item1, c2 As Integer = p2.Item2
                Dim p3 As Tp = tmp3(t3)
                Dim d3 As Integer = p3.Item1, c3 As Integer = p3.Item2

                Dim rotU As Integer = 0
                Dim rotD As Integer = 0
                Dim sw12 As Integer = d3
                Dim sw23 As Integer = d1
                Dim sw31 As Integer = d2
                Dim d12 As Integer = 0, d13 As Integer = 0
                Dim d23 As Integer = 0, d21 As Integer = 0
                Dim d31 As Integer = 0, d32 As Integer = 0
                If c1 >= 0 Then
                    d21 = Distance(sX2, sY2, cusXs(c1), cusYs(c1))
                    d31 = Distance(sX3, sY3, cusXs(c1), cusYs(c1))
                    rotU += d21
                    rotD += d31
                    sw12 += d21
                    sw31 += d31
                End If
                If c2 >= 0 Then
                    d32 = Distance(sX3, sY3, cusXs(c2), cusYs(c2))
                    d12 = Distance(sX1, sY1, cusXs(c2), cusYs(c2))
                    rotU += d32
                    rotD += d12
                    sw12 += d12
                    sw23 += d32
                End If
                If c3 >= 0 Then
                    d13 = Distance(sX1, sY1, cusXs(c3), cusYs(c3))
                    d23 = Distance(sX2, sY2, cusXs(c3), cusYs(c3))
                    rotU += d13
                    rotD += d23
                    sw23 += d23
                    sw31 += d13
                End If

                Dim minD As Integer = d1 + d2 + d3
                Dim sel As Integer = -1
                If rotU < minD Then
                    minD = rotU
                    sel = 0
                End If
                If rotD < minD Then
                    minD = rotD
                    sel = 1
                End If
                If sw12 < minD Then
                    minD = sw12
                    sel = 2
                End If
                If sw23 < minD Then
                    minD = sw23
                    sel = 3
                End If
                If sw31 < minD Then
                    minD = sw31
                    sel = 4
                End If
                Select Case sel
                Case 0
                    tmp1(t1) = If(c3 < 0, dummy, New Tp(d13, c3))
                    tmp2(t2) = If(c1 < 0, dummy, New Tp(d21, c1))
                    tmp3(t3) = If(c2 < 0, dummy, New Tp(d32, c2))
                    exitFlag = False
                Case 1
                    tmp1(t1) = If(c2 < 0, dummy, New Tp(d12, c2))
                    tmp2(t2) = If(c3 < 0, dummy, New Tp(d23, c3))
                    tmp3(t3) = If(c1 < 0, dummy, New Tp(d31, c1))
                    exitFlag = False
                Case 2
                    tmp1(t1) = If(c2 < 0, dummy, New Tp(d12, c2))
                    tmp2(t2) = If(c1 < 0, dummy, New Tp(d21, c1))
                    exitFlag = False
                Case 3
                    tmp2(t2) = If(c3 < 0, dummy, New Tp(d23, c3))
                    tmp3(t3) = If(c2 < 0, dummy, New Tp(d32, c2))
                    exitFlag = False
                Case 4
                    tmp1(t1) = If(c3 < 0, dummy, New Tp(d13, c3))
                    tmp3(t3) = If(c1 < 0, dummy, New Tp(d31, c1))
                    exitFlag = False
                End Select

            Next t3
            Next t2
            Next t1

            If exitFlag Then Exit Do

        Loop

        For Each p As Tp In tmp1
            Dim i As Integer = p.Item2
            If i < 0 Then Continue For
            ret(i) = New Customer(i, k1)
        Next p
        For Each p As Tp In tmp2
            Dim i As Integer = p.Item2
            If i < 0 Then Continue For
            ret(i) = New Customer(i, k2)
        Next p
        For Each p As Tp In tmp3
            Dim i As Integer = p.Item2
            If i < 0 Then Continue For
            ret(i) = New Customer(i, k3)
        Next p

    End Sub

    Class Customer
        Public Id As Integer
        Public Target As Integer
        Public Junction As Boolean = False
        Public Sub New(id As Integer, target As Integer)
            Me.Id = id: Me.Target = target
        End Sub
    End Class

    Private Sub Shuffle(posList() As Integer)
        For i As Integer = 0 To UBound(posList)
            Dim j As Integer = rand.Next(i, posList.Length)
            Dim tmp As Integer = posList(i)
            posList(i) = posList(j)
            posList(j) = tmp
        Next i
    End Sub

    Private Function MultipleStart(posList() As Integer, origCusList() As Customer, limTim As Integer) As Tuple(Of List(Of TpIIIs), Customer(), Integer)
        Shuffle(posList)

        Dim cusList(UBound(origCusList)) As Customer
        For i As Integer = 0 To UBound(cusList)
            Dim c As Customer = origCusList(i)
            cusList(i) = New Customer(c.Id, c.Target)
        Next i

        Dim trucks As New List(Of TpIIIs)()
        Dim truckTime As Integer = Math.Min(limTim, Environment.TickCount + 1000)
        For i As Integer = 0 To UBound(posList)
            Dim time1 As Integer = Environment.TickCount
            If time1 > truckTime Then
                Exit For
            End If
            Dim pos As Integer = posList(i)
            Dim pX As Integer = GetX(pos)
            Dim pY As Integer = GetY(pos)
            Dim cnt(warehouseIds.Count - 1) As List(Of Customer)
            Dim bCosts(warehouseIds.Count - 1) As Integer
            Dim aCosts(warehouseIds.Count - 1) As Integer
            For Each c As Customer In cusList
                If c.Junction Then Continue For
                Dim d1 As Integer = Distance(GetX(c.Target), GetY(c.Target), cusXs(c.Id), cusYs(c.Id))
                Dim d2 As Integer = Distance(pX, pY, cusXs(c.Id), cusYs(c.Id))
                If d1 <= d2 Then Continue For
                Dim wid As Integer = warehouseIds(c.Target)
                If cnt(wid) Is Nothing Then cnt(wid) = New List(Of Customer)()
                cnt(wid).Add(c)
                bCosts(wid) += d1
                aCosts(wid) += d2
            Next c
            For j As Integer = 0 To UBound(cnt)
                If cnt(j) Is Nothing Then Continue For
                Dim sX As Integer = GetX(warehousePos(j))
                Dim sY As Integer = GetY(warehousePos(j))
                Dim d As Integer = Distance(sX, sY, pX, pY)
                If bCosts(j) <= fCost + vCost * d + aCosts(j) Then Continue For
                Dim cs As New List(Of Integer)
                For Each c As Customer In cnt(j)
                    c.Junction = True
                    c.Target = pos
                    cs.Add(c.Id)
                Next c
                trucks.Add(New TpIIIs(warehousePos(j), pos, cs.ToArray()))
            Next j
        Next i

        Dim totalCost As Integer = 0

        For Each c As Customer In cusList
            If c.Junction Then Continue For
            Dim sX As Integer = GetX(c.Target)
            Dim sY As Integer = GetY(c.Target)
            totalCost += Distance(sX, sY, cusXs(c.Id), cusYs(c.Id))
        Next c

        For i As Integer = 0 To trucks.Count - 1
            Dim t As TpIIIs = trucks(i)
            Dim sX As Integer = GetX(t.Item1)
            Dim sY As Integer = GetY(t.Item1)
            Dim eX As Integer = GetX(t.Item2)
            Dim eY As Integer = GetY(t.Item2)
            Dim selX As Integer = eX, selY As Integer = eY
            Dim selCost As Integer = vCost * Distance(sX, sY, eX, eY)
            For Each cid As Integer In t.Item3
                selCost += Distance(eX, eY, cusXs(cid), cusYs(cid))
            Next cid
            For q As Integer = 0 To 1000
                For j As Integer = 0 To UBound(dxs)
                    Dim x As Integer = eX + dxs(j)
                    Dim y As Integer = eY + dys(j)
                    Dim tmpCost As Integer = vCost * Distance(sX, sY, x, y)
                    For Each cid As Integer In t.Item3
                        tmpCost += Distance(x, y, cusXs(cid), cusYs(cid))
                    Next cid
                    If tmpCost < selCost Then
                        selCost = tmpCost
                        selX = x
                        selY = y
                    End If
                Next j
                If selX = eX AndAlso selY = eY Then Exit For
                eX = selX
                eY = selY
            Next q
            totalCost += selCost
            Dim pos As Integer = MakeKey(selX, selY)
            If pos = t.Item2 Then Continue For
            trucks(i) = New TpIIIs(t.Item1, pos, t.Item3)
            For Each cid As Integer In t.Item3
                cusList(cid).Target = pos
            Next cid
        Next i

        MultipleStart = New Tuple(Of List(Of TpIIIs), Customer(), Integer)(trucks, cusList, totalCost)
    End Function


End Class