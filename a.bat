@echo off
if "%~1"=="" goto defaultseed
java -jar tester.jar -exec "Main.exe" -seed %*
@GOTO finally
:defaultseed
java -jar tester.jar -exec "Main.exe" -seed 1
@GOTO finally
:finally
